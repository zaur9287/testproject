
export const USER = {
    ADD_USER: 'ADD_USER',
    UPDATE_USER: 'UPDATE_USER',
    DELETE_USER: 'DELETE_USER'
};

export function addUser(newUser) {
    return {
        type: USER.ADD_USER,
        payload: {
            user: newUser
        }
    };
}

export function updateUser(updateData) {
    return {
        type: USER.UPDATE_USER,
        payload: {
            user: updateData
        }
    };
}

export function deleteUser(userInfo) {
    return {
        type: USER.DELETE_USER,
        payload: {
            user: userInfo
        }
    };
}
