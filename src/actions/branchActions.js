
export const BRANCH = {
    ADD_BRANCH: 'ADD_BRANCH',
    UPDATE_BRANCH: 'UPDATE_BRANCH',
    DELETE_BRANCH: 'DELETE_BRANCH'
};

export function addBranch(newBranch) {
    return {
        type: BRANCH.ADD_BRANCH,
        payload: {
            branch: newBranch
        }
    };
}

export function updateBranch(updateData) {
    return {
        type: BRANCH.UPDATE_BRANCH,
        payload: {
            branch: updateData
        }
    };
}

export function deleteBranch(branchInfo) {
    return {
        type: BRANCH.DELETE_BRANCH,
        payload: {
            branch: branchInfo
        }
    };
}
