export function getNewID(email) {
    for (var i = 0, text=''; i < email.length; i++) {
        text += email.charCodeAt(email[i]).toString();
    }
    return text;
}

export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}