import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import App from './components/App';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers/index';

const initialState = {
    branches:[
        {
            id: '5k34hj52k4',
            name: 'Branch 1',
            phoneNumber: '12 123 45 67',
            address: 'New York No. 1 Lake Park',
            owner: 'Qudamov Agaseyfeddin',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda atque deserunt dolorem eius fuga hic magnam nisi odit voluptatibus!'
        },
        {
            id: '34j2hk4hj2k3',
            name: 'Branch 1',
            phoneNumber: '12 111111',
            address: 'New York No. 1 Lake Park',
            owner: 'Alabulali Isfendiyar',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet aperiam aspernatur blanditiis error ex expedita molestias neque velit voluptatem.'
        }
    ],
    users: [
        {
            id: '4j45l3j4k52',
            fullName: 'Cabbar Elizade',
            email: 'maksud@test.com',
            password: '111',
            phoneNumber: '12 123 45 67',
            address: 'New York No. 1 Lake Park',
            tags: ['Developer', 'Administrator'],
            website: 'asan.gov.az'
        },
        {
            id: 'jh2j4h5l234j',
            fullName: 'Maktun Turalov',
            email: 'tual@test.com',
            password: '111',
            phoneNumber: '12 111111',
            address: 'New York No. 1 Lake Park',
            tags: ['Sales'],
            website: 'asan.gov.az'
        }
    ]
};

const store = createStore(reducers, initialState);


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('root'));
