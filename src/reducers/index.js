import { combineReducers } from 'redux';
import userReducer from '../reducers/userReducer';
import branchReducer from '../reducers/branchReducer';

export default combineReducers({
        users: userReducer,
        branches: branchReducer
    });