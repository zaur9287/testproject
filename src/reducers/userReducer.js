import { USER } from '../actions/userActions';
import * as globals from '../global/Globals';

const userReducer = (state = [], {type, payload}) => {
    console.log('state ', state);
    console.log('payload ', payload);
    switch (type) {
        case USER.ADD_USER:
            return [...state,
                {
                    ...payload.user,
                    id: globals.getNewID(payload.user.email)
                }
            ];

        case USER.DELETE_USER:

            return [...state.filter(user => user.id !== payload.user.id)];

        case USER.UPDATE_USER:
            let updatedUsers = state.map(user => {
                if (user.id === payload.user.id) {
                    user = {
                        ...user,
                        ...payload.user,
                        phoneNumber: payload.user.prefix + payload.user.phone
                    };
                }
                return user;
            });

            return updatedUsers;

        default:
            return state;
    }
    console.log(state);
};

export default userReducer;
