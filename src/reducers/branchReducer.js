import { BRANCH } from '../actions/branchActions';


const branchReducer = (state = [], action) => {
    switch (action.type) {
        case BRANCH.ADD_BRANCH:
            return {
                ...state,
                branches: [...state.branches, action.payload.branch]
            };

        case BRANCH.DELETE_BRANCH:
            return {
                ...state,
                branches: state.branches.filter(branch => branch.id !== action.payload.branch.id)
            };

        case BRANCH.UPDATE_BRANCH:
            const { branchData } = action.payload;
            let updatedBranches = state.users.map(branch => {
                if (branch.id === action.payload.branch.id) {
                    branch = {
                        ...branch,
                        ...branchData,
                    };
                }
                return branch;
            });

            return {
                ...state,
                branches: updatedBranches
            };

        default:
            return state;
    }
};

export default branchReducer;
