import React from 'react';
import { Layout, Menu, Icon, Col, Row, Dropdown, Empty,  } from 'antd';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import './App.scss';

import { connect } from 'react-redux';

import * as routes from './routes';

import Dashboard from './pages/Dashboard';
import Company from './pages/Company';
import UserProfile from './pages/users/UserProfile';
import Users from './pages/users/Users';
import Branches from './pages/Branches';
import Support from './pages/Support';
import Report from './pages/Report';

const { Footer, Sider, } = Layout;
const SubMenu = Menu.SubMenu;

const Advertisement = () => { return (<h1>Advertisement page</h1>); };
const Job = () => {
    return (
        <Row justify="center" align="middle" style={{height: '100%'}} >
            <Col style={{margin: '0 auto'}}>
                <h1 style={{textAlign: 'center'}}>Jobs page</h1>
                <br/><br/><br/>
                <Empty />
            </Col>
        </Row>);
};

const footer = (
    <Footer style={{ bottom: 0, textAlign: 'center' }}>
        Copyright ©2019 Created by Zaur.
    </Footer>
);

const menuUser = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="#">Account</a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="#">Logout</a>
        </Menu.Item>
    </Menu>
);

class App extends React.Component {
    constructor(props) {
        super();
        this.addUser = this.addUser.bind(this);
        this.updateUser = this.updateUser.bind(this);
    }

    state = {
        collapsed: false,
        selectedPage: ['1'],
    };

    addUser(newUser) {
      const { users } = this.state;
      users.push(newUser);
      this.setState({
          users: users
      });
    };

    updateUser(updateData){
        const { users } = this.state;
        let updatedUsers = users.map(user => {
            if (user.id == updateData.id) {
                user = {
                    ...user,
                    ...updateData,
                    phoneNumber: updateData.prefix + updateData.phone
                };
            }
            return user;
        });

        this.setState({
            users: updatedUsers
        });

    };

    deleteUser = (userID) => {
        this.setState({
            users: this.state.users.filter(user => user.id !== userID)
        });
    };

    onCollapse = (collapsed) => {
        console.log(collapsed);
        this.setState({ collapsed });
    };

    updateSelected = (selected) => {
        this.setState({
            ...this.state,
            selectedPage: selected
        });
        console.log(this.state);
        console.log(selected);
    };

    clickedMenu = ({ item, key, selectedKeys}) => {
        this.setState({
            ...this.state,
            selectedPage: selectedKeys
        });
        //globals.setCookie("lastMenuPage", selectedKeys, 10);
    };

    render() {
        const { users } = this.props;
        return (
            <Router>
                <Layout style={{ minHeight: '100vh' }}>
                    <Sider
                        breakpoint="md"
                        onBreakpoint={(broken) => { console.log(broken); }}
                        collapsible
                        collapsed={this.state.collapsed}
                        onCollapse={this.onCollapse}
                    >
                        <div className="logo" />
                        <Menu theme="dark" defaultSelectedKeys={this.state.selectedPage} mode="inline" onSelect={this.clickedMenu}>
                            <Menu.Item key="1">
                                <Icon type="dashboard" />
                                <span><NavLink exact to={routes.dashboard} className="navlink-items" activeClassName="navlink-activeClass-items">Dashboard</NavLink></span>
                            </Menu.Item>
                            <Menu.Item key="2">
                                <Icon type="compass" />
                                <span><NavLink exact to={routes.company} className="navlink-items" activeClassName="navlink-activeClass-items">Company</NavLink></span>
                            </Menu.Item>
                            <SubMenu
                                key="sub1"
                                title={<span><Icon type="user" />
                                    <span>
                                        <NavLink exact to={routes.users} className="navlink-items" activeClassName="navlink-activeClass-items">Users</NavLink>
                                    </span>
                                </span>}
                            >
                                {
                                    users.map(user => {
                                        return (
                                        <Menu.Item key={user.id}>
                                            <NavLink
                                                exact
                                                to={routes.users + '/' + user.id}
                                                className="navlink-items"
                                                activeClassName="navlink-activeClass-items">
                                                {user.fullName}

                                                </NavLink>
                                        </Menu.Item>);
                                    })
                                }

                            </SubMenu>
                            <Menu.Item key="8">
                                <Icon type="align-left" />
                                <span>
                                    <NavLink exact to={routes.branches} className="navlink-items" activeClassName="navlink-activeClass-items">Branches</NavLink>
                                </span>
                            </Menu.Item>
                            <Menu.Item key="11">
                                <Icon type="build" />
                                <span><NavLink exact to={routes.job} className="navlink-items" activeClassName="navlink-activeClass-items">Job</NavLink></span>
                            </Menu.Item>
                            <Menu.Item key="12">
                                <Icon type="audit" />
                                <span><NavLink exact to={routes.report} className="navlink-items" activeClassName="navlink-activeClass-items">Report</NavLink></span>
                            </Menu.Item>
                            <Menu.Item key="13">
                                <Icon type="question-circle" />
                                <span><NavLink exact to={routes.support} className="navlink-items" activeClassName="navlink-activeClass-items">Support</NavLink></span>
                            </Menu.Item>
                        </Menu>
                    </Sider>

                    <Layout>
                        <Row gutter={16}>
                            <Col className="gutter-row" xs={24} lg={{ span: 8, offset: 16 }} align="right" justify="center" style={{color: 'white'}}>
                                <Dropdown overlay={menuUser}>
                                    <a className="ant-dropdown-link" href="#" style={{color: '#868e96'}}>
                                        <Icon type="user"/> Zaur Narimanli &nbsp;
                                    </a>
                                </Dropdown>
                            </Col>
                        </Row>


                        <Route path={routes.dashboard} exact  component={Dashboard} />
                        <Route path={routes.company} exact  component={Company} />
                        {
                            users.map(user => {
                                return ( <Route path={routes.users + '/' + user.id} exact key={user.id}  render={() => { return <UserProfile updateUser={this.updateUser} user={user}/>}} />);
                            })
                        }
                        <Route path={routes.users} exact  render={() => {
                            return (<Users users={users} addUser={this.addUser} deleteUser={this.deleteUser} updateSelected={this.updateSelected}/>);
                        }} />
                        <Route path={routes.branches} exact  component={Branches} />
                        <Route path={routes.advertisement} exact  component={Advertisement} />
                        <Route path={routes.job} exact  component={Job} />
                        <Route path={routes.report} exact  component={Report} />
                        <Route path={routes.support} exact  component={Support} />

                        {footer}
                    </Layout>

                </Layout>
            </Router>
        );
    }
}

const mapStateToProps = state => {
    return ({users: state.users})
};

export default connect(mapStateToProps)(App);
