import React, {Component} from 'react';
import {
    Row, Col
} from 'antd';
import SimpleChart from './report/SimpleChart';
import Donut from './report/Donut';


class Report extends Component {
    render() {
        return (
            <Row justify="center" align="middle">
                <Col md={12} sm={24} xs={24}>
                    {<SimpleChart/>}
                </Col>
                <Col md={12} sm={24} xs={24}>
                    {<Donut/>}
                </Col>
            </Row>
        );
    }
}

export default Report;
