import React from 'react';
import {
    Row, Col,
    Card, Avatar

} from 'antd';

const Support = () =>
            <Row style={{margin: '0 auto'}} align="middle" justify="space-between" gutter={48}>
                <Col md={24} sm={24} xs={24}>
                    <h1>Support Contacts & Addresses</h1>
                    <p className="lead">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium delectus eveniet excepturi, fuga, officia praesentium quaerat quisquam quos repellendus repudiandae sint vel velit voluptate? Dignissimos earum libero pariatur, porro quos vel voluptate. Laborum magnam, minima nisi odit pariatur praesentium soluta tempore unde voluptates! Amet cupiditate doloremque maxime minus nesciunt nobis quod ratione saepe veniam, voluptates? Delectus dignissimos dolorem et fuga fugit, ipsa laboriosam odio perferendis perspiciatis quasi quo saepe sint veritatis! Aperiam exercitationem incidunt nihil quas veritatis? A, accusamus atque consequuntur enim eum neque nisi numquam omnis placeat porro quisquam vitae. Doloribus eaque, eum eveniet magnam modi necessitatibus quis reiciendis!
                    </p>
                    <p className="lead">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda delectus dolores doloribus facere iure reiciendis tempora ullam vero. Blanditiis omnis quae quasi! Aliquid, blanditiis, consequatur dolor dolore enim error eum excepturi illo iste mollitia quaerat quo quos saepe sapiente sed suscipit temporibus! Accusantium alias architecto aspernatur commodi ducimus eius eligendi, et eveniet expedita harum iste, iusto modi molestiae mollitia nisi officia perferendis porro quo quod sint sunt ullam velit vero. Architecto assumenda dolor ducimus ex nesciunt nihil omnis ratione? Aperiam et, fugit nemo odio quidem ratione sed suscipit tempora! Asperiores at, beatae delectus dignissimos et explicabo illo, illum impedit iste laudantium, libero magni maxime natus nemo nulla obcaecati officia omnis quae quaerat quas quibusdam quis quo repellat saepe temporibus veniam?
                    </p>
                    <Row gutter={12}>
                        <Col md={12} sm={24} xs={24}>
                            <Card style={{ width: '100%', marginTop: 10 }}>
                                <Card.Meta
                                    avatar={
                                        <Avatar
                                            src="https://www.randomaddressgenerator.com/media/face/male33.jpg"
                                            size="large"
                                        />
                                    }
                                    title="Joseph Hansen"
                                    description={
                                        <div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis ducimus error incidunt ipsum reprehenderit suscipit temporibus. Deserunt dicta eius quisquam? Animi deleniti eaque fuga illum qui quia quisquam tempore tenetur!</p>
                                            <address>
                                                <a href="mailto:marzuq@mycasualclothing.com">Write a letter</a><br/>
                                                Visit us at: <a href="https://www.mycasualclothing.com/">mycasualclothing.com</a><br/>
                                                948659 Pioneer Boulevard  Norwalk, CA 90650<br/>
                                                USA
                                            </address>
                                        </div>
                                    }
                                />
                            </Card>
                            <Card style={{ width: '100%', marginTop: 10 }}>
                                <Card.Meta
                                    avatar={
                                        <Avatar
                                            src="https://www.randomaddressgenerator.com/media/face/male54.jpg"
                                            size="large"
                                        />
                                    }
                                    title="Anthony Hill"
                                    description={
                                        <div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio, quos!</p>
                                            <address>
                                                <a href="mailto:groggies@galaxys8giveaway.us">Write a letter</a><br/>
                                                Visit us at: <a href="https://www.galaxys8giveaway.us/">galaxys8giveaway.us</a><br/>
                                                580 West Monroe Street Chicago, IL 60606<br/>
                                                USA
                                            </address>
                                        </div>
                                    }
                                />
                            </Card>
                        </Col>
                        <Col md={12} sm={24} xs={24}>
                            <Card style={{ width: '100%', marginTop: 10 }}>
                                <Card.Meta
                                    avatar={
                                        <Avatar
                                            src="https://www.randomaddressgenerator.com/media/face/female50.jpg"
                                            size="large"
                                        />
                                    }
                                    title="Lisa Thompson"
                                    description={
                                        <div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda illo omnis quam sapiente tempore temporibus.</p>
                                            <address>
                                                <a href="mailto:predinne@h0116.top">Write a letter</a><br/>
                                                Visit us at: <a href="https://www.h0116.top/">h0116.top</a><br/>
                                                1851 Chambers Drive Northwest Conyers, GA 30012<br/>
                                                USA
                                            </address>
                                        </div>
                                    }
                                />
                            </Card>
                        </Col>
                    </Row>

                </Col>
            </Row>;

export default Support;
