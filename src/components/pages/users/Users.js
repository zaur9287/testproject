import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import { Row, Col, Table, Divider, Tag, Popconfirm } from 'antd';
import * as routes from "../../routes";
import NewUser from '../NewUser';
import { connect } from 'react-redux';
import { deleteUser } from "../../../actions/userActions";

const { Column } = Table;

class Users extends Component {
    constructor(props) {
        super(props);
        this.deleteUser = this.deleteUser.bind(this);
    }

    deleteUser(user) {
        this.props.dispatch(deleteUser(user));
    }
    clickedMenu = (name) => {
        this.props.updateSelected(name)
    };

    render() {
        return (
            <Row>
                <Row type="flex" justify="space-around" align="middle" style={{minHeigt: 42}}>
                    <Col span={8} style={{paddingLeft: 48}}><h1>Users Page</h1></Col>
                    <Col span={8}></Col>
                    <Col span={8} align={"right"} style={{paddingRight: 48}}>
                        <NewUser addUser={this.props.addUser}/>
                    </Col>
                </Row>
                <Row>
                    <Col span={22} offset={1}>
                        <Table dataSource={this.props.users}>
                            <Column
                                title="Full name"
                                dataIndex="fullName"
                                key="fullName"
                            />
                            <Column
                                title="Email"
                                dataIndex="email"
                                key="email"
                            />
                            <Column
                                title="Address"
                                dataIndex="address"
                                key="address"
                            />
                            <Column
                                title="Phone number"
                                dataIndex="phoneNumber"
                                key="phoneNumber"
                            />
                            <Column
                                title="Website"
                                dataIndex="website"
                                key="website"
                            />
                            <Column
                                title="Tags"
                                dataIndex="tags"
                                key="tags"
                                render={tags => (
                                    <span>
                                      {tags.map(tag => <Tag color="blue" key={tag}>{tag}</Tag>)}
                                    </span>
                                )}
                            />
                            <Column
                                title="Action"
                                key="action"
                                render={(text, record) => (
                                    <span>
                                        <NavLink exact to={routes.users + '/' + record.id} style={{color: '#28a745'}} onClick={() => this.clickedMenu([record.id])}>Edit</NavLink>
                                        <Divider type="vertical" />
                                        <Popconfirm title="Are you sure？" okText="Yes" cancelText="No" onConfirm={() => this.deleteUser(record)}>
                                            <a href="#">Delete</a>
                                        </Popconfirm>
                                    </span>
                                )}
                            />
                        </Table>
                    </Col>
                </Row>
            </Row>
        );
    }
}

export default connect()(Users);
