import React from 'react';
import {
    Form, Input, Select, AutoComplete,
} from 'antd';
import {defaultTags} from "../../../global/defaults";

const { Option } = Select;
const AutoCompleteOption = AutoComplete.Option;

const children = [];
for (let i = 0; i < defaultTags.length; i++) {
    children.push(<Option key={defaultTags[i]}>{defaultTags[i]}</Option>);
}

class NewRegistrationForm extends React.Component {
    constructor(props) {
        super();
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            user: {
                id: '',
                fullName: '',
                email: '',
                password: '',
                phoneNumber: '',
                address: '',
                tags: [],
                website: ''
            }
        };
        this.handleValues = this.handleValues.bind(this);
        this.handleTags = this.handleTags.bind(this);
    }



    handleValues(event) {
        this.setState({
            user:{
                ...this.state.user,
                [event.target.id]: event.target.value
            }
        });
        this.props.saveValues({ ...this.state.user });
    };

    handleTags(values) {
        this.setState({
            user:{
                ...this.state.user,
                tags: values
            }
        });
        this.props.saveValues({ ...this.state.user });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    }

    handleWebsiteChange = (value) => {
        let autoCompleteResult;
        if (!value) {
            autoCompleteResult = [];
        } else {
            autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
        }
        this.setState({ autoCompleteResult });
    }

    render() {
        const { user } = this.state;
        const { getFieldDecorator } = this.props.form;
        const { autoCompleteResult } = this.state;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };

        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '994',
        })(
            <Select style={{ width: 70 }}>
                <Option value="995">+995</Option>
                <Option value="7">+7</Option>
            </Select>
        );

        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item {...formItemLayout}  label="Full name" >
                    {getFieldDecorator('fullName', {
                        rules: [{ type: 'text',
                        }, { required: true, message: 'Please input your Full name!', }],
                        initialValue: user.email
                    })( <Input onChange={this.handleValues}/> )}
                </Form.Item>
                <Form.Item {...formItemLayout}  label="E-mail" >
                    {getFieldDecorator('email', {
                        rules: [{ type: 'email', message: 'The input is not valid E-mail!',
                        }, { required: true, message: 'Please input your E-mail!', }],
                        initialValue: user.email
                    })( <Input onChange={this.handleValues}/> )}
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Password"
                >
                    {getFieldDecorator('password', {
                        rules: [{
                            required: true, message: 'Please input your password!',
                        }, {
                            validator: this.validateToNextPassword,
                        }],
                        initialValue: user.password
                    })(
                        <Input type="password" onChange={this.handleValues}/>
                    )}
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Confirm Password"
                >
                    {getFieldDecorator('confirm', {
                        rules: [{
                            required: true, message: 'Please confirm your password!',
                        }, {
                            validator: this.compareToFirstPassword,
                        }],
                        initialValue: user.password
                    })(
                        <Input type="password" onBlur={this.handleConfirmBlur} />
                    )}
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Phone Number"
                >
                    {getFieldDecorator('phoneNumber', {
                        rules: [{ required: true, message: 'Please input your phone number!' }],
                        initialValue: user.phoneNumber
                    })(
                        <Input addonBefore={prefixSelector} style={{ width: '100%' }}  onChange={this.handleValues} />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout}  label="Address" >
                    {getFieldDecorator('address', {
                        rules: [{ type: 'text', message: 'The input your address!',
                        }, { required: true, message: 'Please input your address!', }],
                        initialValue: user.address
                    })( <Input onChange={this.handleValues}/> )}
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Website"
                >
                    {getFieldDecorator('website', {
                        rules: [{ required: false, message: 'Please input website!' }],
                        initialValue: user.website
                    })(
                        <Input onChange={this.handleValues} />
                    )}
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Tags"
                >
                    {getFieldDecorator('tags', {
                        rules: [{ required: false}],
                        initialValue: user.tags
                    })(
                        <Select
                            mode="tags"
                            style={{ width: '100%' }}
                            placeholder="Choose Tags"
                            onChange={this.handleTags}
                        >
                            {children}
                        </Select>
                    )}

                </Form.Item>
            </Form>
        );
    }
}

export default Form.create({ name: '' })(NewRegistrationForm);;