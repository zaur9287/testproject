import React, {Component} from 'react';
import {
    Row, Col,
} from 'antd';
import RegistrationForm from './RegistrationForm';



class UserProfile extends Component {
    render() {
        return (
            <Row>
                <Row>
                    <Col span={24} align={'center'}>
                        <img src="http://lorempixel.com/200/200/sports" alt="" style={{borderRadius: '50%'}}/>
                        <br/>
                        <br/>
                        <h1>{this.props.user.fullName}</h1>
                        <h3>{this.props.user.tags.join(" ").toUpperCase()}</h3>
                    </Col>
                </Row>
                <Row>
                    <Col span={2}></Col>
                    <Col span={16}>
                        <RegistrationForm updateUser={this.props.updateUser} user={this.props.user}/>
                    </Col>
                    <Col span={6}></Col>
                </Row>
            </Row>
        );
    }
}

export default UserProfile;
