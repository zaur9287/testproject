import React, {Component} from 'react';
import { Row, Col, Icon, Button, Avatar, Card} from 'antd';
import './Company.css';

class Company extends Component {
    render() {
        return (

            <Row>
                <Row style={{ fontSize:32, marginBottom: 10}}>
                    <Col span={12} style={{color: '#28a745', paddingLeft:10}}>Company page</Col>
                    <Col span={12} align="right" style={{paddingRight:10}}>
                        <Button type="danger" size="large">
                            Edit
                            <Icon type="edit"/>
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col span={4} align={"center"}>
                        <img src="http://lorempixel.com/128/128/business/" alt=""/>
                    </Col>
                    <Col span={20}>
                        <h1>My New Company</h1>
                        <Button icon="copyright" type="dashed"><code>[ Add Website ]</code></Button><br/>
                        <Button icon="phone"><code>[ + Add Phone Number ]</code></Button>
                        <Button icon="phone"><code>[ + Add Fax ]</code></Button>
                        <Button icon="environment"><code>[ + Add Location ]</code></Button><br/>
                        <br/>
                        <hr/>
                        <Row>
                            <Col span={12}>
                                <h2 className="company-page-owner-information">Owner Information</h2>
                                <br/>
                                <Card>
                                    <Card.Meta
                                        avatar={<Avatar src="http://lorempixel.com/128/128/cats/" size="large" />}
                                        title="Agsaqqal Dirmanbeyov"
                                        description="Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, recusandae!"
                                    />
                                </Card>

                            </Col>
                            <Col span={12}>
                                <h2 className="company-page-owner-information">Address Information</h2>
                                <br/>
                                <Button><code>[ + Add Street ]</code></Button><br/>
                                <Button><code>[ + Add City ]</code></Button>
                                <Button><code>[ + Add State ]</code></Button><br/>
                                <Button><code>[ + Add Postal Code ]</code></Button>
                                <Button><code>[ + Add Country ]</code></Button><br/>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={12}>
                                <br/>
                                <h2 className="company-page-owner-information" >Regional Information</h2>
                                <br/>
                                <Button icon="edit"><code>[ Change Language ]</code></Button><br/>
                                <Button icon="edit"><code>[ Change Time Zone ]</code></Button><br/>
                            </Col>
                        </Row>
                    </Col>
                </Row>

            </Row>
        );
    }
}

export default Company;
