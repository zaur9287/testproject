import React, {Component} from 'react';
import { Row, Col, Table, Tag, Popconfirm, Icon } from 'antd';
import Branch from './Branch';
import NewBranch from './NewBranch';
import * as globals from "../../global/Globals";
import { connect } from 'react-redux';

const { Column } = Table;


class Branches extends Component {
    constructor(props) {
        super();
        this.deleteUser = this.deleteUser.bind(this);
        this.addBranch = this.addBranch.bind(this);
    }

    addBranch(branch){
        const { branches } = this.state;
        branch.id = globals.getNewID(branch.name);
        branches.push(branch);
        this.setState({
            branches
        });
        console.log(this.state.branches);
    };

    deleteUser(selectedBranch) {
        this.setState({
            branches: this.state.branches.filter(branch => branch.id !== selectedBranch.id)
        });
    }

    render() {
        const { branches } = this.props;
        return (
            <Row>
                <Row type="flex" justify="space-around" align="middle" style={{minHeigt: 42}}>
                    <Col span={8} style={{paddingLeft: 48}}><h1>Branches Page</h1></Col>
                    <Col span={8}></Col>
                    <Col span={8} align={"right"} style={{paddingRight: 48}}>
                        <NewBranch addBranch={this.addBranch}/>
                    </Col>
                </Row>
                <Row>
                    <Col span={22} offset={1}>
                        <Table dataSource={branches}>
                            <Column
                                title="Name"
                                dataIndex="name"
                                key="name"
                            />
                            <Column
                                title="Owner"
                                dataIndex="owner"
                                key="owner"
                            />
                            <Column
                                title="Address"
                                dataIndex="address"
                                key="address"
                            />
                            <Column
                                title="Phone number"
                                dataIndex="phoneNumber"
                                key="phoneNumber"
                            />
                            <Column
                                title="Action"
                                key="action"
                                render={(text, record) => (
                                    <span>
                                        <Branch branch={record}/>
                                        <Popconfirm title="Are you sure？" okText="Yes" cancelText="No" onConfirm={() => this.deleteUser(record)}>
                                            <Tag onClick={this.showDrawer} style={{ color: '#dc3545', borderStyle: 'solid' }}>
                                                <Icon type="delete" /> Delete
                                            </Tag>
                                        </Popconfirm>
                                    </span>
                                )}
                            />
                        </Table>
                    </Col>
                </Row>
            </Row>
        );
    }
}
const mapStateToProps = state => {
    return ({branches: state.branches});
};
export default connect(mapStateToProps)(Branches);
