import React, {Component} from 'react';

import { Modal,
    Row, Col, Button, Input
} from 'antd';
import NewRegistrationForm from './users/NewRegistrationForm';
import * as globals from '../../global/Globals';
import {message} from "antd/lib/index";
import { connect} from 'react-redux';
import { addUser } from "../../actions/userActions";

class NewUser extends Component {
    constructor(props) {
        super();
        this.saveValues = this.saveValues.bind(this);
    }

    state = {
        visible: false,
        user:{}
    }

    saveValues(newValues){
        this.setState({
            user: newValues
        });
    };

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = (e) => {
        message.success('User created!', 3);
        this.props.dispatch(addUser({
            ...this.state.user,
            id: globals.getNewID(this.state.user.email)
        }));
        this.setState({
            visible: false,
            user: {}
        });
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }


    render() {
        return (
            <Row>
                <Button type="primary" onClick={this.showModal}>
                    Add User
                </Button>
                <Modal
                    title={'New User: ' + this.state.user.fullName}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <Row>
                        <Col span={24}>
                            {<NewRegistrationForm saveValues={this.saveValues}/>}
                        </Col>
                    </Row>
                </Modal>

            </Row>
        );
    }
}

export default connect()(NewUser);