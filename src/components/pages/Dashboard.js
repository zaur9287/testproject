import React, {Component} from 'react';
import { Card, Icon, Col, Row,Statistic } from 'antd';

class Dashboard extends Component {
    render() {
        return (
            <Row style={{margin: '0 auto'}}>
                <Row gutter={8} style={{marginTop: 10,}}>
                    <Col xs={24} sm={12} md={6}>
                        <Card style={{ width: 250 }} align="center" >
                            <h1>Today's sales</h1>
                            <h2>$5050</h2>
                        </Card>
                    </Col>
                    <Col xs={24} sm={12} md={6}>
                        <Card style={{ width: 250, backgroundColor: '#17a2b8' }} align="center" >
                            <h1>Last Week Fee</h1>
                            <h2>$320</h2>
                        </Card>
                    </Col>
                    <Col xs={24} sm={12} md={6}>
                        <Card style={{ width: 250 }} align="center" >
                            <h1>Customers</h1>
                            <h2>165</h2>
                        </Card>
                    </Col>
                    <Col xs={24} sm={12} md={6}>
                        <Card style={{ width: 250, backgroundColor: '#ffc107' }} align="center" >
                            <h1>Discounts</h1>
                            <h2>$398</h2>
                        </Card>
                    </Col>
                </Row>
                <Row justify="space-between" style={{width: '100%', marginTop: 15}} >
                    <Col span={12}>
                        <Card>
                            <Statistic
                                title={<h3>Income difference with yesterday</h3>}
                                value={11.28}
                                precision={2}
                                valueStyle={{ color: '#3f8600' }}
                                prefix={<Icon type="arrow-up" />}
                                suffix="%"
                            />
                        </Card>
                    </Col>
                    <Col span={12}>
                        <Card>
                            <Statistic
                                title={<h3>Discount difference with last week</h3>}
                                value={9.3}
                                precision={2}
                                valueStyle={{ color: '#cf1322' }}
                                prefix={<Icon type="arrow-down" />}
                                suffix="%"
                            />
                        </Card>
                    </Col>
                </Row>
            </Row>

        )}
}

export default Dashboard;
