import React from 'react';
import {
    Drawer, Form, Button, Col, Row, Input, Select, message, Icon, Tag
} from 'antd';

const { Option } = Select;

class DrawerForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleNewBranchValues = this.handleNewBranchValues.bind(this);
        this.handleNewBranchValueOwner = this.handleNewBranchValueOwner.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    state = {
        visible: false,
        branch: {
            id: null,
            name: '',
            phoneNumber: '',
            address: '',
            owner: '',
            description: ''
        }
    };

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        message.success('Branch created!', 3);
        this.setState({
            visible: false,
            branch: {
                id: null,
                name: '',
                phoneNumber: '',
                address: '',
                owner: '',
                description: ''
            }
        });
    };

    handleNewBranchValues(event) {
        console.log(this.state.branch);
        this.setState({
            branch:{
                ...this.state.branch,
                [event.target.id]: event.target.value
            }
        });
    };

    handleNewBranchValueOwner(value) {
        this.setState({
            branch:{
                ...this.state.branch,
                owner: value
            }
        });
    };

    onSubmit() {
      this.props.addBranch({
          ...this.state.branch
      });
      this.onClose();
    };

    render() {
        const { branch } = this.state;
        const { getFieldDecorator } = this.props.form;

        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '994',
        })(
            <Select style={{ width: 70 }}>
                <Option value="994">+994</Option>
                <Option value="7">+7</Option>
            </Select>
        );
        return (
            <div>
                <Tag  onClick={this.showDrawer}  style={{ color: '#007bff', borderStyle: 'dashed' }} >
                    <Icon type="plus" /> Add New
                </Tag>

                <Drawer
                    title={"Edit " + branch.name}
                    width={400}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    style={{
                        overflow: 'auto',
                        height: 'calc(100% - 108px)',
                        paddingBottom: '108px',
                    }}
                >
                    <Form layout="vertical" hideRequiredMark>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Name">
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please enter user name' }],
                                        initialValue: branch.name,
                                    })(<Input placeholder="Please enter user name" onChange={this.handleNewBranchValues}/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Owner">
                                    {getFieldDecorator('owner', {
                                        rules: [{ required: false, message: 'Please select an owner' }],
                                        initialValue: branch.owner,
                                    })(
                                        <Select placeholder="Please select an owner" onChange={this.handleNewBranchValueOwner}>
                                            <Option value="Agappagli Turdistan">Agappagli Turdistan</Option>
                                            <Option value="Qapqarali Canbulaq">Qapqarali Canbulaq</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Address">
                                    {getFieldDecorator('address', {
                                        rules: [{ required: true, message: 'Please enter address' }],
                                        initialValue: branch.address
                                    })(<Input placeholder="Please enter address"  onChange={this.handleNewBranchValues}/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Phone number">
                                    {getFieldDecorator('phoneNumber', {
                                        rules: [{ required: true, message: 'Please input your phone number!' }],
                                        initialValue: branch.phoneNumber
                                    })(
                                        <Input addonBefore={prefixSelector} style={{ width: '100%' }}  onChange={this.handleNewBranchValues}/>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Description">
                                    {getFieldDecorator('description', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'please enter url description',
                                            },
                                        ],
                                        initialValue: branch.description
                                    })(<Input.TextArea rows={4} placeholder="please enter url description"  onChange={this.handleNewBranchValues}/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                    <div
                        style={{
                            position: 'absolute',
                            left: 0,
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e9e9e9',
                            padding: '10px 16px',
                            background: '#fff',
                            textAlign: 'right',
                        }}
                    >
                        <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                            Cancel
                        </Button>
                        <Button onClick={this.onSubmit} type="primary">
                            Submit
                        </Button>
                    </div>
                </Drawer>
            </div>
        );
    }
}

export default Form.create()(DrawerForm);