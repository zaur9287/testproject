import React from 'react';
import {
    Drawer, Form, Button, Col, Row, Input, Select, Icon, Tag
} from 'antd';

const { Option } = Select;


class DrawerForm extends React.Component {
    state = { visible: false };

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    render() {
        const { branch } = this.props;
        const { getFieldDecorator } = this.props.form;

        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '994',
        })(
            <Select style={{ width: 70 }}>
                <Option value="995">+995</Option>
                <Option value="7">+7</Option>
            </Select>
        );
        return (
            <div>
                <Tag  onClick={this.showDrawer}  style={{ color: '#007bff', borderStyle: 'dashed' }} >
                    <Icon type="edit" /> Edit
                </Tag>

                <Drawer
                    title={"Edit " + this.props.branch.name}
                    width={400}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    style={{
                        overflow: 'auto',
                        height: 'calc(100% - 108px)',
                        paddingBottom: '108px',
                    }}
                >
                    <Form layout="vertical" hideRequiredMark>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Name">
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please enter user name' }],
                                        initialValue: branch.name
                                    })(<Input placeholder="Please enter user name" />)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Owner">
                                    {getFieldDecorator('owner', {
                                        rules: [{ required: true, message: 'Please select an owner' }],
                                        initialValue: branch.owner
                                    })(
                                        <Select placeholder="Please select an owner">
                                            <Option value="xiao">Agappagli Turdistan</Option>
                                            <Option value="mao">Qapqarali Canbulaq</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Address">
                                    {getFieldDecorator('address', {
                                        rules: [{ required: true, message: 'Please enter address' }],
                                        initialValue: branch.address
                                    })(<Input placeholder="Please enter address" />)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Phone number">
                                    {getFieldDecorator('phone', {
                                        rules: [{ required: true, message: 'Please input your phone number!' }],
                                        initialValue: branch.phoneNumber
                                    })(
                                        <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Description">
                                    {getFieldDecorator('description', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'please enter url description',
                                            },
                                        ],
                                        initialValue: this.props.branch.description
                                    })(<Input.TextArea rows={4} placeholder="please enter url description" />)}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                    <div
                        style={{
                            position: 'absolute',
                            left: 0,
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e9e9e9',
                            padding: '10px 16px',
                            background: '#fff',
                            textAlign: 'right',
                        }}
                    >
                        <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                            Cancel
                        </Button>
                        <Button onClick={this.onClose} type="primary">
                            Submit
                        </Button>
                    </div>
                </Drawer>
            </div>
        );
    }
}

const Branch = Form.create()(DrawerForm);
export default Branch;